package server.costs;

import java.math.BigDecimal;


public class CallingPhotosAllCosts {
    private BigDecimal callingOnePhotoCost;
    private Integer numberOfPhotos;
    private BigDecimal shippingCost;

    public CallingPhotosAllCosts() {
    }

    public CallingPhotosAllCosts(BigDecimal costOfCallingPhoto, Integer numberOfPhotos, BigDecimal shippingCost) {
        this.callingOnePhotoCost = costOfCallingPhoto;
        this.numberOfPhotos = numberOfPhotos;
        this.shippingCost = shippingCost;
    }


    public BigDecimal getCostSum() {
        BigDecimal costSum;
        costSum = callingOnePhotoCost.multiply(new BigDecimal(numberOfPhotos));
        costSum = costSum.add(shippingCost);
        return costSum;
    }

    public BigDecimal getCallingOnePhotoCost() {
        return callingOnePhotoCost;
    }

    public void setCallingOnePhotoCost(BigDecimal callingOnePhotoCost) {
        this.callingOnePhotoCost = callingOnePhotoCost;
    }

    public Integer getNumberOfPhotos() {
        return numberOfPhotos;
    }

    public void setNumberOfPhotos(Integer numberOfPhotos) {
        this.numberOfPhotos = numberOfPhotos;
    }

    public BigDecimal getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(BigDecimal shippingCost) {
        this.shippingCost = shippingCost;
    }
}

