package server;

import server.costs.CallingPhotosAllCosts;
import sun.rmi.runtime.Log;

import java.io.*;
import java.math.BigDecimal;
import java.net.Socket;

class Connection extends Thread {
    private Socket socket;
    private CallingPhotosAllCosts callingPhotosAllCosts;
    private String sumOfCosts;

    public Connection(Socket accept) {
        this.socket = accept;
    }

    public void run() {
        try {
            InputStream inputStream = this.socket.getInputStream();
            OutputStream outputStream = this.socket.getOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String messageFromClient;
            String jsonMessage = null;
            int state = 0;

            while ((messageFromClient = bufferedReader.readLine()) != null) {
                System.out.println("Message from client: " + messageFromClient);
                if (messageFromClient.equalsIgnoreCase("SENDING JSON") && state == 0) {
                    state++;
                    printState(state);
                    dataOutputStream.writeBytes("OK\r");
                } else if (state == 1) {
                    state++;
                    printState(state);
                    jsonMessage = messageFromClient;
                    sumOfCosts = getSumOfCosts(jsonMessage);
                    dataOutputStream.writeBytes("GOOD\r");
                } else if ((messageFromClient.equalsIgnoreCase("GET COSTS") && state == 2)) {
                    state++;
                    printState(state);
                    dataOutputStream.writeBytes(sumOfCosts + "\r");
                } else if ((messageFromClient.equalsIgnoreCase("END")) && state == 3) {
                    state++;
                    printState(state);
                    break;
                }
            }

            dataOutputStream.close();
            bufferedReader.close();
            System.out.println("Communication Ended");


        } catch (IOException var6) {
            var6.printStackTrace();
        }
    }

    private void printState(int state) {
        System.out.println("Server state: " + state);
    }

    private String getSumOfCosts(String jsonMessage) {
        String sum = "1000"; // this is a default value.
        BigDecimal sumOfCosts = new BigDecimal(sum);
        if (jsonMessage != null) {
            ClientMessageAdapter clientMessageAdapter = new ClientMessageAdapter(jsonMessage);
            callingPhotosAllCosts = new CallingPhotosAllCosts(
                    clientMessageAdapter.getCostOfCallPhoto(),
                    clientMessageAdapter.getNumOfPhotos(),
                    clientMessageAdapter.getShippingCost());
            sumOfCosts = callingPhotosAllCosts.getCostSum();
        }

        sum = String.valueOf(sumOfCosts);
        return String.valueOf(sum);
    }
}