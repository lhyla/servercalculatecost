package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public Server() {
    }

    public static void main(String[] args) {
        try {
            ServerSocket e = new ServerSocket(8888);
            while (true) {
                Socket accept = e.accept();
                Connection thread = new Connection(accept);
                thread.start();
            }
        } catch (IOException var4) {
            var4.printStackTrace();
        }
    }
}