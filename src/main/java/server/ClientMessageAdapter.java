package server;

import org.json.JSONObject;

import java.math.BigDecimal;
//The Class is created only to parse clientMessage to JsonObject and return the parts of the message.

public class ClientMessageAdapter {

    private String clientMessage;
    private JSONObject jsonObject;

    public ClientMessageAdapter(String clientMessage) {
        this.clientMessage = clientMessage;
        jsonObject = new JSONObject(clientMessage);
    }

    public Integer getNumOfPhotos() {
        return (Integer) jsonObject.get("numberOfPhotos");
    }

    public BigDecimal getShippingCost() {
        return new BigDecimal((Double) jsonObject.get("shippingCost"));
    }

    public BigDecimal getCostOfCallPhoto() {
        return new BigDecimal((Double) jsonObject.get("costOfCallOnePhoto"));
    }
}