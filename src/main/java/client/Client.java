package client;

import org.json.JSONObject;

import java.io.*;
import java.math.BigDecimal;
import java.net.Socket;

public class Client {
    public Client() {
    }

    public static void main(String[] args) {

        //These parameters should be taken from application context, here is example where are created in code.
        Integer numOfPhotos = 20;
        BigDecimal shippingCost = new BigDecimal(22.5);
        BigDecimal costOfCallOnePhoto = new BigDecimal(13.5);
        String sumOfCosts = null;

        JSONObject jsonObject = new JSONObject();

        //For example jsonObject collect this data from this class, but finally it can do it from whole project
        //from different modules.

        jsonObject.put("numberOfPhotos", numOfPhotos);
        jsonObject.put("shippingCost", shippingCost);
        jsonObject.put("costOfCallOnePhoto", costOfCallOnePhoto);
        String message = jsonObject.toString();

        try {
            Socket socket = new Socket("localhost", 8888);
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);

            dataOutputStream.writeBytes("SENDING JSON\r");

            String messageFromServer;
            int state = 0;

            while ((messageFromServer = bufferedReader.readLine()) != null) {
                System.out.println("Message form server: " + messageFromServer);
                if (messageFromServer.equalsIgnoreCase("OK") && state == 0) {
                    state++;
                    printState(state);
                    //System.out.println("JSON Message: " + message+"\r");
                    dataOutputStream.writeBytes(message + "\r");
                } else if (messageFromServer.equalsIgnoreCase("GOOD") && state == 1) {
                    state++;
                    printState(state);
                    dataOutputStream.writeBytes("GET COSTS\r");
                } else if (state == 2) {
                    state++;
                    printState(state);
                    sumOfCosts = messageFromServer;
                    dataOutputStream.writeBytes("END\r");
                } else if (messageFromServer.equalsIgnoreCase("END") && state == 3) {
                    state++;
                    printState(state);
                    break;
                }
            }

            System.out.println("Sum of cost: " + sumOfCosts);
            bufferedReader.close();
            dataOutputStream.close();
            System.out.println("Communication Ended");
        } catch (IOException var9) {
            var9.printStackTrace();
        }
    }

    private static void printState(int state) {
        System.out.println("Client state: " + state);
    }
}
